const {Post} = require('./../models')
const request = require('request');
//const req = require("express");
const {Op} = require('sequelize')
const fs = require("fs");


module.exports = {
    getDataFromLink: async (req, res) => {
        request({
            method: 'GET',
            uri: 'https://jsonplaceholder.typicode.com/comments',
        }, async function (error, response, body) {
            if (error) {
                console.log(error);
                return;
            }
            const data = response.body;
            const apiData = JSON.parse(data)
            await Post.bulkCreate(apiData)
            res.send({
                message: 'success',
                status: 1
            })
            if (response.statusCode === 200) {
                console.log('success');
            } else {
                console.log("error with api call")
            }
        });
    },

    searchPost: async (req, res) => {
        let requestPrams = req.body
        let query = {}
        if (requestPrams.search) {
            query = {
                [Op.or]: [
                    { 'name': { [Op.like]: '%' + requestPrams.search + '%' } },
                    { 'email': { [Op.like]: '%' + requestPrams.search + '%' } },
                    { 'body': { [Op.like]: '%' + requestPrams.search + '%' } }
                ]
            }
        }

        await Post.findAndCountAll({
            where:query,
            limit: requestPrams.limit,
            order: [[requestPrams.sort ? requestPrams.sort : 'createdAt', 'ASC']]
        }).then((data) => {
            if (data.count > 0) {
                res.send({
                    data: data.rows,
                    count: data.count,
                    limit: requestPrams.limit
                })
            }
        })
    },

    saveDataFromCSV: async (req,res) => {
        let csv= require('fast-csv');

        var stream = fs.createReadStream('./csvdata.csv');

        csv.parseStream(stream, {headers : true})
            .on("data", async function(data){
                console.log(data);
                let inserObj = {
                    id: data.id,
                    postId: data.postid,
                    name: data.name,
                    email: data.email,
                    body: data.body,
                }
                await Post.create(inserObj)

            })
            .on("end", function(){
                console.log("done");
            });
        res.send({
            message: 'success',
            status: 1
        })
    }
}