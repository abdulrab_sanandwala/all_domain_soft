const router = require('express').Router()
const controllerObj = require('./controller')


router.get('/getDataFromLink', controllerObj.getDataFromLink)
router.post('/searchPost', controllerObj.searchPost)
router.get('/saveDataFromCSV', controllerObj.saveDataFromCSV)

module.exports = router

