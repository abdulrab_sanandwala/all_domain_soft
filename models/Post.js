const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
    class Post extends Model {}

    Post.init(
        {
            postId: DataTypes.INTEGER,
            name: DataTypes.STRING,
            email: DataTypes.STRING,
            body: DataTypes.TEXT
        },
        {
            sequelize,
            timestamps: true,
            modelName: 'Post',
            tableName: 'post'
        }
    )

    return Post
}
