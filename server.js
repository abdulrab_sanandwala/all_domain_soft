const express = require('express')
const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', '*')
    res.header('Access-Control-Allow-Methods', 'GET, POST')
    next()
})

const indexRoute = require('./src/routes')
app.use('/', indexRoute)

const port = process.env.PORT || 4000
app.listen(port, () => {
    console.log(`server running at port ${port}`)
})